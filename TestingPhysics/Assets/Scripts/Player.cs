﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour
{

    public float maxJumpHeight = 4;
    public float minJumpHeight = 1;
    public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    float moveSpeed = 6;

    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;

    public float wallSlideSpeedMax = 3;
    public float wallStickTime = .25f;
    float timeToWallUnstick;

    float gravity;
    float maxJumpVelocity;
    float minJumpVelocity;
    Vector3 velocity;
    float velocityXSmoothing;

    Controller2D controller;
<<<<<<< HEAD
    public GameObject clawPrefab;
    private GameObject clawObject;
    public Animation clawAnimation;


=======

    //4testing
    bool DrawGizmos = false;
    float cooldown = 0.5f;
    float timer = 0;
    //-----------
>>>>>>> ee78505274c631680c4e4add36f83b7e69d489c7

    void Start()
    {
        controller = GetComponent<Controller2D>();

        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
        print("Gravity: " + gravity + "  Jump Velocity: " + maxJumpVelocity);
<<<<<<< HEAD

        
=======
>>>>>>> ee78505274c631680c4e4add36f83b7e69d489c7
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        int wallDirX = (controller.collisions.left) ? -1 : 1;

        float targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);

        bool wallSliding = false;
        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {
            wallSliding = true;

            if (velocity.y < -wallSlideSpeedMax)
            {
                velocity.y = -wallSlideSpeedMax;
            }

            if (timeToWallUnstick > 0)
            {
                velocityXSmoothing = 0;
                velocity.x = 0;

                if (input.x != wallDirX && input.x != 0)
                {
                    timeToWallUnstick -= Time.deltaTime;
                }
                else
                {
                    timeToWallUnstick = wallStickTime;
                }
            }
            else
            {
                timeToWallUnstick = wallStickTime;
            }

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (wallSliding)
            {
<<<<<<< HEAD
=======
                print(input.x);
>>>>>>> ee78505274c631680c4e4add36f83b7e69d489c7
                if (wallDirX == input.x)
                {
                    velocity.x = -wallDirX * wallJumpClimb.x;
                    velocity.y = wallJumpClimb.y;
                }
                else if (input.x == 0)
                {
                    velocity.x = -wallDirX * wallJumpOff.x;
                    velocity.y = wallJumpOff.y;
                }
                else
                {
                    velocity.x = -wallDirX * wallLeap.x;
                    velocity.y = wallLeap.y;
                }
            }
            if (controller.collisions.below)
            {
                velocity.y = maxJumpVelocity;
            }
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (velocity.y > minJumpVelocity)
            {
                velocity.y = minJumpVelocity;
            }
        }
<<<<<<< HEAD
        if(Input.GetKeyUp(KeyCode.Z))
        {
            clawObject = Instantiate(clawPrefab) as GameObject;
            clawObject.transform.SetParent(gameObject.transform ,false);

            //play animation here
            clawAnimation.Play();

            StartCoroutine(DestroyClaw());
        }
=======
>>>>>>> ee78505274c631680c4e4add36f83b7e69d489c7


        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime, input);

<<<<<<< HEAD
=======
        if (Input.GetKey(KeyCode.S))
        {
            Slam();
        }

>>>>>>> ee78505274c631680c4e4add36f83b7e69d489c7
        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }

    }

<<<<<<< HEAD
    IEnumerator DestroyClaw()
    {
        yield return new WaitForSeconds(1f); //This would ideally be synced up with swipe animation
        Destroy(clawObject);
    }
=======
    //==========================================
    void Roar()
    {
        
    }

    void Slam()
    {
        Ray2D ray = new Ray2D();
        ray.origin = transform.position - Vector3.up * transform.localScale.y * 0.5f;
        ray.direction = Vector2.down;
        RaycastHit2D info = Physics2D.Raycast(ray.origin, ray.direction, 0.1f, controller.collisionMask);//!!!NOTE: will break with unusual scale
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * 0.2f);

        if (info.collider != null)
        {
            if (velocity.y <= -40)//!!!PROTO: hardcoded
            {
                DrawGizmos = true;
                timer = cooldown;
            }
        }
        else
        {
            velocity.y = Mathf.Max(-50, velocity.y - 300 * Time.deltaTime);
        }
    }

    void OnDrawGizmos()
    {
        if (DrawGizmos)
        {
            timer -= Time.deltaTime;

            Gizmos.color = new Color(0, 1, 0, .5f);
            Gizmos.DrawSphere(transform.position, 2);
        }

        DrawGizmos = timer > 0 ? true : false;
    }
    //==========================================
>>>>>>> ee78505274c631680c4e4add36f83b7e69d489c7
}

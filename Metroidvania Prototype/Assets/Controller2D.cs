﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Controller2D : MonoBehaviour
{
    const float skinWidth = .015f;
    public int horizontalRayCount = 4;
    public int verticalRayCount = 4;

    float horizontalRaySpacing;
    float verticalRaySpacing;

    BoxCollider2D boxCollider;
    RaycastOrigins raycastOrigins;


    struct RaycastOrigins
    {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    }

    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        UpdateRaycastOrigins();
        CalculateRaySpacing();

        for(int i = 0; i < verticalRayCount; i++)
        {
            Debug.DrawRay(raycastOrigins.bottomLeft + Vector2.right * verticalRaySpacing * i, Vector2.up * -2, Color.red);
        }
    }

    void UpdateRaycastOrigins()
    {
        Bounds boxBounds = GetComponent<Collider>().bounds;

        boxBounds.Expand(skinWidth * -2);

        raycastOrigins.bottomLeft = new Vector2(boxBounds.min.x, boxBounds.min.y);
        raycastOrigins.bottomRight = new Vector2(boxBounds.max.x, boxBounds.min.y);

        raycastOrigins.topLeft = new Vector2(boxBounds.min.x, boxBounds.max.y);
        raycastOrigins.topRight = new Vector2(boxBounds.max.x, boxBounds.max.y);
    }


    void CalculateRaySpacing()
    {
        Bounds boxBounds = GetComponent<Collider>().bounds;
        boxBounds.Expand(skinWidth * -2);

        horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
        verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);

        horizontalRaySpacing = boxBounds.size.y / (horizontalRayCount - 1);
        verticalRaySpacing = boxBounds.size.x / (verticalRayCount - 1);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform cameraTarget;

    private bool smooth = true;
    private float smoothSpeed = 0.125f;
    private Vector3 cameraOffset = new Vector3(0, 0, -6.5f);

    public PlayerController transitionCamera;

    // Use this for initialization
    void Start ()
    { 

    }
	
	// Update is called once per frame
	private void FixedUpdate ()
    {
        

        Vector3 desiredPosition = cameraTarget.transform.position + cameraOffset;

        if(smooth)
        {
            transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        }
        else
            transform.position = desiredPosition;

        if(transitionCamera.inCombat)
        {
            Vector3 fullCamera = cameraTarget.transform.position + (new Vector3(0,0,-50.0f));

            transform.position = Vector3.Lerp(transform.position, fullCamera, smoothSpeed);
        }

        


	}
}
